import argparse
import html2text
import itertools
import json
import logging
import os
import os.path
import requests
from bs4 import BeautifulSoup
from collections import namedtuple
from slugify import slugify

logging.basicConfig(level=logging.INFO)
logging.getLogger("urllib3").setLevel(logging.WARNING)

Page = namedtuple('Page', 'title content published_at images tags')

def parse_pages(json_file):
    data = json.load(json_file)['db'][0]['data']
    page_tags = parse_tags(data['tags'], data['posts_tags'])
    return [parse_page(p, page_tags) for p in data['posts']]

def parse_tags(tags_json, page_tags_json):
    tags = { tag['id']: tag['name'] for tag in tags_json }
    resolved_page_tags = {}
    grouped_page_tags = itertools.groupby(page_tags_json, lambda page_tag: page_tag['post_id'])
    for page_id, page_tags in grouped_page_tags:
        resolved_page_tags[page_id] = [tags[page_tag['tag_id']] for page_tag in page_tags]
    return resolved_page_tags

def parse_page(page_json, page_tags):
    page_id = page_json["id"]
    return Page(
        title = page_json["title"],
        content = parse_html(page_json["html"]),
        published_at = page_json["published_at"],
        images = parse_images(page_json["html"]),
        tags = page_tags[page_id] if page_id in page_tags else []
    )

def parse_images(html_content):
    soup = BeautifulSoup(html_content, 'html.parser')
    img_tags = soup.find_all('img')
    return [img.get('src') for img in img_tags]

def parse_html(html_content):
    soup = BeautifulSoup(html_content, 'html.parser')
    img_tags = soup.find_all('img')
    for img in img_tags:
        img['src'] = os.path.basename(img.get('src'))
    return str(soup)

def create_page(page, site_url, collocate):
    slug = slugify(page.title)
    if collocate:
        if not os.path.exists(slug):
            os.makedirs(slug)
        output_filename = os.path.join(slug, 'index.md')
    else:
        output_filename = slug + '.md'

    download_images(page, slug, site_url)
    write_markdown(page, output_filename)

def write_markdown(page, output_filename):
    logging.info("Writing markdown to {}".format(output_filename))

    with open(output_filename, 'w') as markdown_file:
        write_page_front_matter(page, markdown_file)
        write_page_content(page, markdown_file)

def write_page_front_matter(page, markdown_file):
    markdown_file.write('+++\n')
    markdown_file.write('title = "{}"\n'.format(page.title))
    if page.published_at is None:
        markdown_file.write('date = 1970-01-01\n')
        markdown_file.write('draft = true\n')
    else:
        markdown_file.write('date = {}\n'.format(page.published_at[:10]))

    if len(page.tags) > 0:
        markdown_file.write('\n[taxonomies]\n')
        markdown_file.write('tags = {}\n'.format(page.tags))

    markdown_file.write('+++\n')

def write_page_content(page, markdown_file):
    converter = html2text.HTML2Text()
    converter.mark_code = True
    converter.bypass_tables = True
    converter.body_width = 80
    converter.wrap_links = False
    converter.wrap_list_items = True
    markdown_content = converter.handle(page.content)
    markdown_content = clean_markdown_content(markdown_content)
    markdown_file.write(markdown_content)

def clean_markdown_content(markdown_content):
    markdown_content = markdown_content.replace('[code]', '```')
    markdown_content = markdown_content.replace('[/code]', '```')
    markdown_content = '\n'.join([line.rstrip() for line in markdown_content.split('\n')])
    return markdown_content

def download_images(page, output_path, site_url):
    for image in page.images:
        url = site_url + image
        image_path = os.path.join(output_path, os.path.basename(image))

        logging.info("Downloading image '{}' to '{}'".format(url, image_path))

        if os.path.exists(image_path):
            logging.info("Using cached image {}".format(image_path))
            continue

        r = requests.get(url, verify=False)
        if r.status_code != 200:
            logging.error("Failed to download {}".format(url))
            continue

        with open(image_path, 'wb') as f:
            for chunk in r:
                f.write(chunk)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('files', type=argparse.FileType('r'), nargs='+')

    args = parser.parse_args()
    for _file in args.files:
        logging.info("Parsing {}".format(_file.name))
        pages = parse_pages(_file)
        logging.info("Found {} pages".format(len(pages)))
        for page in pages:
            create_page(page, site_url="https://jonblack.me", collocate=True)
